package com.example.bookStore.mapper;

import com.example.bookStore.dto.AuthorDto;
import com.example.bookStore.dto.CreateAuthorDto;
import com.example.bookStore.model.Author;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-24T21:23:41+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.9 (Amazon.com Inc.)"
)
@Component
public class AuthorMapperImpl implements AuthorMapper {

    @Override
    public Author dtoToAuthor(CreateAuthorDto createAuthorDto) {
        if ( createAuthorDto == null ) {
            return null;
        }

        Author author = new Author();

        return author;
    }

    @Override
    public AuthorDto authorToDto(Author author) {
        if ( author == null ) {
            return null;
        }

        AuthorDto authorDto = new AuthorDto();

        return authorDto;
    }
}
