package com.example.bookStore.mapper;

import com.example.bookStore.dto.BookDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.model.Book;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-24T21:23:42+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.9 (Amazon.com Inc.)"
)
@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public Book dtoToBook(CreateBookDto createBookDto) {
        if ( createBookDto == null ) {
            return null;
        }

        Book book = new Book();

        return book;
    }

    @Override
    public BookDto bookToDto(Book book) {
        if ( book == null ) {
            return null;
        }

        BookDto bookDto = new BookDto();

        return bookDto;
    }
}
