package com.example.bookStore.mapper;

import com.example.bookStore.dto.CreateStudentDto;
import com.example.bookStore.dto.StudentDto;
import com.example.bookStore.model.Student;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-24T21:23:41+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.9 (Amazon.com Inc.)"
)
@Component
public class StudentMapperImpl implements StudentMapper {

    @Override
    public Student dtoToStudent(CreateStudentDto createStudentDto) {
        if ( createStudentDto == null ) {
            return null;
        }

        Student student = new Student();

        return student;
    }

    @Override
    public StudentDto studentToDto(Student student) {
        if ( student == null ) {
            return null;
        }

        StudentDto studentDto = new StudentDto();

        return studentDto;
    }
}
