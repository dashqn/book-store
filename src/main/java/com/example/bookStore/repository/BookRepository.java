package com.example.bookStore.repository;

import com.example.bookStore.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookRepository extends JpaRepository<Book , Long> {
    Optional<java.awt.print.Book> findByName(String name);

    Page<Book> findAll(Specification<Book> and);
}
