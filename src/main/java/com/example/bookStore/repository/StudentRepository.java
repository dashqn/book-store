package com.example.bookStore.repository;

import com.example.bookStore.model.Author;
import com.example.bookStore.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Long> {
    Optional<Student> findByName(String name);
}
