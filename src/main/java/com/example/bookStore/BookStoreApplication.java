package com.example.bookStore;

import com.example.bookStore.model.Author;
import com.example.bookStore.model.Book;
import com.example.bookStore.repository.AuthorRepository;
import com.example.bookStore.repository.BookRepository;
import com.example.bookStore.service.AuthorService;
import com.example.bookStore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@RequiredArgsConstructor
@SpringBootApplication
public class BookStoreApplication implements CommandLineRunner {
	private final AuthorService authorService;
	private final BookService bookService;
	private final BookRepository bookRepository;
	private final AuthorRepository authorRepository;

	public static void main(String[] args) {
		SpringApplication.run(BookStoreApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Author author = authorRepository.findById(1L).get();
		System.out.println(author);
		Book book1 = Book.builder()
				.name("book1")
				.author(author)
				.build();
	    Book book2 = Book.builder()
				.name("book2")
			    .author(author)
				.build();
		bookRepository.save(book1);
        bookRepository.save(book2);
	}
}

