package com.example.bookStore.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CreateAuthorDto {
    @NotBlank
    String name;
    @NotBlank
    Integer age;
    List<CreateBookDto> books;
}
