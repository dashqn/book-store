package com.example.bookStore.mapper;

import com.example.bookStore.dto.BookDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.model.Book;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BookMapper {
    Book dtoToBook(CreateBookDto createBookDto);
    BookDto bookToDto(Book book);
}
