package com.example.bookStore.mapper;

import com.example.bookStore.dto.CreateStudentDto;
import com.example.bookStore.dto.StudentDto;
import com.example.bookStore.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
    Student dtoToStudent(CreateStudentDto createStudentDto);
    StudentDto studentToDto(Student student);
}
