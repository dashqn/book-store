package com.example.bookStore.mapper;

import com.example.bookStore.dto.AuthorDto;
import com.example.bookStore.dto.CreateAuthorDto;
import com.example.bookStore.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthorMapper {
    Author dtoToAuthor(CreateAuthorDto createAuthorDto);

    AuthorDto authorToDto(Author author);
}
