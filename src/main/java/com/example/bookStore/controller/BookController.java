package com.example.bookStore.controller;

import com.example.bookStore.dto.BookDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.genericsearch.SearchCriteria;
import com.example.bookStore.model.Book;
import com.example.bookStore.service.BookService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/author/{authorId}/book")
@Valid
public class BookController {
    private final BookService bookService;
    public void create(@RequestBody CreateBookDto bookDto){
        bookService.create(bookDto);
    }
    @PostMapping("/search")
    public Page<Book> search(@PathVariable Long authorId,
                             @RequestBody List<SearchCriteria> searchCriteria,
                             Pageable pageable){
        return bookService.searchByName(authorId,searchCriteria,pageable);
    }
    @PutMapping("/{bookId}")
    public void updateBook(@PathVariable Long id,
                           @RequestBody CreateBookDto createBookDto){
        bookService.update(id, createBookDto);
    }
    @GetMapping("/{id}")
    public BookDto findById(@PathVariable Long id){
        return bookService.findById(id);
    }
    @GetMapping
    public List<Book> findAll(){
        return bookService.findAll();
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        bookService.delete(id);
    }
}
