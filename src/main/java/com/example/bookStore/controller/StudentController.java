package com.example.bookStore.controller;

import com.example.bookStore.dto.CreateStudentDto;
import com.example.bookStore.dto.StudentDto;
import com.example.bookStore.service.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student/{studentId}")
@Valid
public class StudentController {
    private final StudentService studentService;
    @PostMapping
    public void create(@RequestBody CreateStudentDto createStudentDto){
        studentService.create(createStudentDto);
    }
    @GetMapping("/{id}")
    public StudentDto findById(@PathVariable Long id){
        return studentService.findById(id);
    }
    @PutMapping("/{id}")
    public void update(@PathVariable Long id,
                       @RequestBody CreateStudentDto createStudentDto){
        studentService.update(id, createStudentDto);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        studentService.delete(id);
    }

}
