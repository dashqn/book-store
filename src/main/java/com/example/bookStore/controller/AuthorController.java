package com.example.bookStore.controller;

import com.example.bookStore.dto.AuthorDto;
import com.example.bookStore.dto.CreateAuthorDto;
import com.example.bookStore.service.AuthorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/author/{authorId}")
@Valid
public class AuthorController {
private final AuthorService authorService;
@PostMapping
public void create(@RequestBody CreateAuthorDto authorDto){
    authorService.create(authorDto);
}
@GetMapping("/{id}")
public AuthorDto findById(@PathVariable Long id){
    return authorService.findById(id);
}
@PutMapping("/{id}")
    public void update(@PathVariable Long id,
                       @RequestBody CreateAuthorDto authorDto){
    authorService.update(id, authorDto);
}

@DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
    authorService.delete(id);
}
}
