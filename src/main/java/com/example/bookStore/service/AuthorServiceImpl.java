package com.example.bookStore.service;

import com.example.bookStore.dto.AuthorDto;
import com.example.bookStore.dto.CreateAuthorDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.mapper.AuthorMapper;
import com.example.bookStore.mapper.BookMapper;
import com.example.bookStore.model.Author;
import com.example.bookStore.repository.AuthorRepository;
import com.example.bookStore.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService{
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final AuthorMapper authorMapper;
    private final BookMapper bookMapper;

    @Override
    public void create(CreateAuthorDto dto) {
        Optional<Author> byName = authorRepository.findByName(dto.getName());
        if(byName.isPresent()){
            throw new RuntimeException();
        }
        Author author = authorMapper.dtoToAuthor(dto);
        authorRepository.save(author);
    }



    @Override
    public AuthorDto findById(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(RuntimeException::new);
        return authorMapper.authorToDto(author);
    }

    @Override
    public void update(Long id, CreateAuthorDto authorDto) {
        authorRepository.findById(id).orElseThrow(RuntimeException::new);
        Author author = authorMapper.dtoToAuthor(authorDto);
        author.setId(id);
        authorRepository.save(author);
        authorMapper.authorToDto(author);
    }

    @Override
    public void delete(Long id) {
      authorRepository.findById(id).orElseThrow(RuntimeException::new);
      bookRepository.deleteById(id);
      delete(id);
    }
}
