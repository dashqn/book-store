package com.example.bookStore.service;

import com.example.bookStore.dto.BookDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.genericsearch.SearchCriteria;
import com.example.bookStore.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    void create(CreateBookDto createBookDto);
    void update(Long id, CreateBookDto createBookDto);
    Page<Book> searchByName(Long authorId, List<SearchCriteria> searchCriteria, Pageable pageable);
    BookDto findById(Long id);
    List<Book> findAll();
    void delete(Long id);

}
