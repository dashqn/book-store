package com.example.bookStore.service;

import com.example.bookStore.dto.CreateStudentDto;
import com.example.bookStore.dto.StudentDto;
import com.example.bookStore.mapper.StudentMapper;
import com.example.bookStore.model.Student;
import com.example.bookStore.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    @Override
    public void create(CreateStudentDto dto) {
        Optional<Student> byName = studentRepository.findByName(dto.getName());
        if (byName.isPresent()){
            throw new RuntimeException();
        }
        Student student = studentMapper.dtoToStudent(dto);
        studentRepository.save(student);
    }

    @Override
    public StudentDto findById(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(RuntimeException::new);
        return studentMapper.studentToDto(student);
    }

    @Override
    public void update(Long id, CreateStudentDto dto) {
        Student student = studentRepository.findById(id).orElseThrow(RuntimeException::new);
        studentMapper.dtoToStudent(dto);
        student.setId(id);
        studentRepository.save(student);
        studentMapper.studentToDto(student);
    }

    @Override
    public void delete(Long id) {
        studentRepository.findById(id).orElseThrow(RuntimeException::new);
        studentRepository.deleteById(id);
        delete(id);
    }
}
