package com.example.bookStore.service;

import com.example.bookStore.dto.CreateStudentDto;
import com.example.bookStore.dto.StudentDto;

public interface StudentService {
    void create (CreateStudentDto createStudentDto);
    StudentDto findById(Long id);
    void update(Long id,CreateStudentDto createStudentDto);
    void delete(Long id);
}
