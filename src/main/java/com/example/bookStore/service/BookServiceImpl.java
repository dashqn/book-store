package com.example.bookStore.service;

import com.example.bookStore.dto.BookDto;
import com.example.bookStore.dto.CreateBookDto;
import com.example.bookStore.genericsearch.CustomSpecification;
import com.example.bookStore.genericsearch.SearchCriteria;
import com.example.bookStore.mapper.BookMapper;
import com.example.bookStore.model.Author;
import com.example.bookStore.model.Book;
import com.example.bookStore.repository.AuthorRepository;
import com.example.bookStore.repository.BookRepository;
import jakarta.persistence.criteria.Join;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final BookMapper bookMapper;


    @Override
    public void create(CreateBookDto dto) {
        Optional<java.awt.print.Book> byName = bookRepository.findByName(dto.getName());
        if(byName.isPresent()){
            throw new RuntimeException();
        }
        com.example.bookStore.model.Book book = bookMapper.dtoToBook(dto);
        bookRepository.save(book);

    }

    @Override
    public void update(Long id, CreateBookDto createBookDto) {
        bookRepository.findById(id).orElseThrow(RuntimeException::new);
        com.example.bookStore.model.Book book = bookMapper.dtoToBook(createBookDto);
        book.setId(id);
        bookRepository.save(book);
        bookMapper.bookToDto(book);
    }

    @Override
    public Page<Book> searchByName(Long authorId, List<SearchCriteria> searchCriteria, Pageable pageable) {
        List<SearchCriteria> newCriteria =searchCriteria.stream()
                .filter(criteria -> !criteria.getKey().equals("name")).toList();
        CustomSpecification<Book> customSpecification= new CustomSpecification<>(newCriteria);
        Specification<Book> authorIdSpec = (root, query, criteriaBuilder) -> {
            Join<Book, Author> bookAuthor = root.join("author");
            return criteriaBuilder.equal(bookAuthor.get("id"), authorId);
        };
       return bookRepository.findAll((Pageable) customSpecification.and(authorIdSpec));
    }

    @Override
    public BookDto findById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);
        return bookMapper.bookToDto(book);
    }


    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        bookRepository.findById(id).orElseThrow(RuntimeException::new);
        bookRepository.deleteById(id);
        delete(id);
    }
}
