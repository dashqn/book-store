package com.example.bookStore.service;

import com.example.bookStore.dto.AuthorDto;
import com.example.bookStore.dto.CreateAuthorDto;
import com.example.bookStore.dto.CreateBookDto;

public interface AuthorService {
    void create (CreateAuthorDto createAuthorDto);
    AuthorDto findById(Long id);
    void update(Long id,CreateAuthorDto createAuthorDto);

    void delete(Long id);
}
